# React.js Fundamentals

This project demonstrates fundamental React.js concepts such as hooks, error handling, form handling, and API calling through a collection of components. Explore each component to understand its functionality.

## Installation

1. Clone the repository.
2. Install dependencies with `npm install`.
3. Start the development server with `npm run dev`.