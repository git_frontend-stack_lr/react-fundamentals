import React, { useState } from 'react';

const ErrorHandlingComponent = () => {
  const [error, setError] = useState(null);

  const simulateError = () => {
    setError(new Error('This is a simulated error'));
  };

  return (
    <div>
      <h2>Error Handling Component</h2>
      <button onClick={simulateError}>Simulate Error</button>
      {error && <p>Error: {error.message}</p>}
    </div>
  );
};

export default ErrorHandlingComponent;
