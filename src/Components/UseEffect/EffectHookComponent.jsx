import React, { useState, useEffect } from 'react';

const EffectHookComponent = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    // Fetch data from API
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(result => setData(result[0]))
      .catch(error => console.error('Error fetching data:', error));
  }, []); // Empty dependency array runs effect only once

  return (
    <div>
      {data ? <p>Data: {data.title}</p> : <p>Loading...</p>}
    </div>
  );
};

export default EffectHookComponent;
