import React from 'react';
import ChildComponent from './ChildComponent';

const ParentComponent = () => {
  const name = "John";
  const age = 30;

  return (
    <div>
      <h2>Parent Component</h2>
      <p>Name: {name}</p>
      <p>Age: {age}</p>
      <ChildComponent name={name} age={age} />
    </div>
  );
};

export default ParentComponent;
