import React from 'react';

const ChildComponent = (props) => {
  const { name, age } = props;

  return (
    <div>
      <h2>Child Component</h2>
      <p>Name: {name}</p>
      <p>Age: {age}</p>
    </div>
  );
};

export default ChildComponent;
