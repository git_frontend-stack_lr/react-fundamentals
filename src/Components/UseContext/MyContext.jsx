// Context.js
import React from 'react';

const MyContext = React.createContext();

export const MyProvider = ({ children }) => {
  const sharedValue = 'Shared value from context';

  return (
    <MyContext.Provider value={sharedValue}>
      {children}
    </MyContext.Provider>
  );
};

export const useMyContext = () => React.useContext(MyContext);
