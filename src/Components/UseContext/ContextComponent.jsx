import React from 'react';
import { useMyContext } from './MyContext';

const ContextComponent = () => {
  const value = useMyContext();

  return (
    <div>
      <p>Value from context: {value}</p>
    </div>
  );
};

export default ContextComponent;
