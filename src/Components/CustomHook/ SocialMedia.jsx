import React from "react";
import useLogicrays from "./useLogicrays";

const SocialMedia = () => {
  const { instagram, linkedin, website } = useLogicrays();
  return (
    <div>
      <p>Instagram ID:<a href={instagram} target="_block">{instagram}</a></p>
      <p>LinkedIn ID: <a href={linkedin} target="_block">{linkedin}</a></p>
      <p>Website :<a href={website} target="_block">{website}</a></p>
    </div>
  );
};

export default SocialMedia;
