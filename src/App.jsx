import "./App.css";
import ApiCallingComponent from "./Components/APICalling /ApiCallingComponent";
import SocialMedia from "./Components/CustomHook/ SocialMedia";
import ErrorHandlingComponent from "./Components/ErrorHandling/ErrorHandlingComponent";
import FormHandlingComponent from "./Components/FornHandle/FormHandlingComponent";
import ParentComponent from "./Components/Props/ParentComponent";
import RoutingComponent from "./Components/Routing/RoutingComponent";
import ContextComponent from "./Components/UseContext/ContextComponent";
import { MyProvider } from "./Components/UseContext/MyContext";
import EffectHookComponent from "./Components/UseEffect/EffectHookComponent";
import RefHookComponent from "./Components/UseRef/RefHookComponent";
import StateHookComponent from "./Components/UseState/StateHookComponent";

function App() {
  return (
    <div>
      <h1>React.js Concepts Showcase</h1>

      <hr />
      <div className="row">
        <div className="col-6">
          <h2>useState Hook Component</h2>
          <StateHookComponent />
        </div>
        <div className="col-16">
          <h2>useEffect Hook Component</h2>
          <EffectHookComponent />
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-6">
          <h2>useContext Hook Component</h2>
          <MyProvider>
            <ContextComponent />
          </MyProvider>
        </div>
        <div className="col-6">
          <h2>useRef Hook Component</h2>
          <RefHookComponent />
        </div>
      </div>
      <hr />

      <div className="row">
        <div className="col-6">
          <h2>Form Handling Component</h2>
          <FormHandlingComponent />
        </div>
        <div className="col-6">
          <h2>API Calling Component</h2>
          <ApiCallingComponent />
        </div>
      </div>
      <hr />

      <div className="row">
        <div className="col-6">
          <h2>Error Handling Component</h2>
          <ErrorHandlingComponent />
        </div>
        <div className="col-6">
          <h2>Routing Component</h2>
          <RoutingComponent />
        </div>
      </div>
      <hr />

      <div className="row">
        <div className="col-6">
          <h2>React Props Example</h2>
          <ParentComponent />
        </div>
        <div className="col-6">
          <h2>Custom hook</h2>
          <SocialMedia />

        </div>
      </div>

      <hr />
    </div>
  );
}

export default App;
